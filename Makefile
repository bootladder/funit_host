CLEANUP = rm -f
MKDIR = mkdir -p

UNITY_ROOT=./link_to_unity_root
HOST_C_COMPILER=gcc
SIZE = arm-none-eabi-size

CFLAGS = -std=gnu99
CFLAGS += -Wall
CFLAGS += -Wextra
CFLAGS += -Werror
CFLAGS += -Wpointer-arith
CFLAGS += -Wcast-align
CFLAGS += -Wwrite-strings
CFLAGS += -Wswitch-default
CFLAGS += -Wunreachable-code
CFLAGS += -Winit-self
CFLAGS += -Wmissing-field-initializers
CFLAGS += -Wno-unknown-pragmas
CFLAGS += -Wstrict-prototypes
CFLAGS += -Wundef
CFLAGS += -Wold-style-definition
CFLAGS += -Wmissing-prototypes
CFLAGS += -Wmissing-declarations
CFLAGS += -DUNITY_FIXTURES

TARGET_PRODUCTION_NAME = funit_host
TARGET_BOOTLOADER_PRODUCTION = $(TARGET_PRODUCTION_NAME).out

SRC_FILES_PRODUCTION=\
  src/Commands.c \
  src/ttySerial.c \
  src/Parse_Bootloader_Commandline.c \
  #src/WriteAppImage.c \

SRC_FILES_PRODUCTION_BOOTLOADER=\
	src/main.c \
	src/LoadFunit.c \

SRC_FILES_TEST = \
  $(UNITY_ROOT)/src/unity.c \
  $(UNITY_ROOT)/extras/fixture/src/unity_fixture.c \
  test/test_runners/all_tests.c \
  test/CommandsTest.c \
  test/Parse_Bootloader_CommandlineTest.c \
  test/test_runners/CommandsTest_Runner.c \
  test/test_runners/Parse_Bootloader_CommandlineTest_Runner.c \
    \

SRC_FILES_TARGET_INTEGRATION_TEST = \
  $(UNITY_ROOT)/src/unity.c \
  $(UNITY_ROOT)/extras/fixture/src/unity_fixture.c \
  test/test_runners/Target_Integration_Tests.c \
  test/AppInterfaceCheckTest.c \
  test/WriteAppImageTest.c \
  test/test_runners/WriteAppImageTest_Runner.c \
  test/test_runners/AppInterfaceCheckTest_Runner.c \
    \

SRC_FILES_MOCK = \
  mock/MockttySerial.c
    \

INC_DIRS=-Isrc \
						 -I$(UNITY_ROOT)/src -I$(UNITY_ROOT)/extras/fixture/src

INC_DIRS_MOCK=-Imock
SYMBOLS=

all: production 

######################################################
production:
	@echo Building Bootloader executable
	$(HOST_C_COMPILER) $(CFLAGS) $(INC_DIRS) $(SYMBOLS) \
			$(SRC_FILES_PRODUCTION)  \
				$(SRC_FILES_PRODUCTION_BOOTLOADER) -o $(TARGET_BOOTLOADER_PRODUCTION) 
	#@cp $(TARGET_BOOTLOADER_PRODUCTION) ../bin/

clean:
	$(CLEANUP) *.o

