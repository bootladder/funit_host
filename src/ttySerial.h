#include <stdint.h>
int ttySerialOpen(char * filename);
int ttySerialClose(void);
int ttySerialWrite(uint8_t * buf, uint16_t size);
int ttySerialRead(uint8_t * buf, uint16_t size);
