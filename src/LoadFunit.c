#include "LoadFunit.h"
#include "Commands.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

static int ReadFile256(char * filename);
static int WriteFile256(char * filename, uint8_t * buf);
static int GetFileLength(char * filename);
static int DoesFileExist(char * filename);

int app_fd = 0;
uint8_t readbuf[256];
uint8_t responsebuf[256];
int numread = 0;
uint32_t addrtrav;

int ExecuteFunit(uint32_t addr, uint32_t param)
{
  if( 1 != Commands_SendExecuteFunit(addr,param) )
  {
    fprintf(stderr,"could not write to serial");
    return 6;
  }
  //Wait for type echo, followed by status code
  if( 2 == Commands_WaitForBytesReceived(responsebuf,2,1000) )
  {
    fprintf(stderr,"timeout waiting for response");
    return 4;
  } 
  if( 5 != responsebuf[0] ){
    fprintf(stderr,"invalid response : %d", responsebuf[0]);
    return 5;
  }
  //Check status code, 1 == success
  if( 1 != responsebuf[1] ){
    fprintf(stderr,"Target Failed to Execute Funit\n");
    return 7;
  }
  fprintf(stderr,"returned status:  %d \n",responsebuf[1]);

  //Read the 4-byte return value
  if( 2 == Commands_WaitForBytesReceived(responsebuf,4,1000) )
  {
    fprintf(stderr,"timeout waiting for response");
    return 4;
  } 

  //print 4 bytes, beginning of responsebuf
  fprintf(stderr,"Funit Return MSB : %02d | %02X \n", responsebuf[0],responsebuf[0]  );
  fprintf(stderr,"Funit Return     : %02d | %02X \n", responsebuf[1],responsebuf[1]  );
  fprintf(stderr,"Funit Return     : %02d | %02X \n", responsebuf[2],responsebuf[2]  );
  fprintf(stderr,"Funit Return LSB : %02d | %02X \n", responsebuf[3],responsebuf[3]  );


  //This is the actual output
  fprintf(stdout,"%c", responsebuf[0]  );
  fprintf(stdout,"%c", responsebuf[1]  );
  fprintf(stdout,"%c", responsebuf[2]  );
  fprintf(stdout,"%c", responsebuf[3]  );

  return responsebuf[1];
}

int LoadFunit(char * filename, uint32_t addr)
{
  if( DoesFileExist(filename) == 0 ){
    fprintf(stderr,"file does not exist: %s",filename);
    return 2;
  }

  if( GetFileLength(filename) > 0x100 ){ //app too long
    fprintf(stderr,"Funit too long: %s", filename);
    return 3;
  }

  if( 0 == ReadFile256(filename) ){
    fprintf(stderr,"could not read 256 from file");
    return 1;
  }

  if( 1 != Commands_SendWriteFlash256(addr,readbuf) )
  {
    fprintf(stderr,"could not write to serial");
    return 6;
  }
  if( 2 == Commands_WaitForBytesReceived(responsebuf,2,1000) )
  {
    fprintf(stderr,"timeout waiting for response");
    return 4;
  } 
  if( 1 != responsebuf[1] ){
    fprintf(stderr,"invalid response : %d|%d", responsebuf[0], responsebuf[1]);
    return 5;
  }

  fprintf(stderr,"Success!");

  return 1;
}
int ReadMemory(uint32_t addr,char * filename)
{
  if( 1 != Commands_SendReadFlash256(addr,readbuf) )
  {
    fprintf(stderr,"could not write to serial");
    return 6;
  }
  if( 2 == Commands_WaitForBytesReceived(responsebuf,2,1000) )
  {
    fprintf(stderr,"timeout waiting for response");
    return 4;
  } 
  if( 1 != responsebuf[1] )
  {
    fprintf(stderr,"invalid response : %d", responsebuf[0]);
    return 5;
  }
  if( 2 == Commands_WaitForBytesReceived(responsebuf,256,1000) )
  {
    fprintf(stderr,"timeout waiting for 256 bytes");
    return 4;
  } 
  
  /* for(int i=0;i<256;i++)printf("%c",responsebuf[i]); */
    
  if( WriteFile256(filename,responsebuf) < 1 ){ //app too long
    fprintf(stderr,"failed writing to filename: %s", filename);
    return 3;
  }

  return 1;
}



static int DoesFileExist(char * filename)
{
  int exists;
  app_fd  = open(filename, O_RDONLY);
  if( app_fd > 0 )
    exists = 1;
  else exists = 0;

  close(app_fd);
  return exists;
}

static int GetFileLength(char * filename)
{
  app_fd  = open(filename, O_RDONLY);
  int size = lseek(app_fd,0L,SEEK_END);
  lseek(app_fd,0,SEEK_SET);
  close(app_fd);
  return size;
}

static int ReadFile256(char * filename)
{
  app_fd  = open(filename, O_RDONLY);
  int ret = read(app_fd, readbuf, 256);
  close(app_fd);
  return ret;
}

static int WriteFile256(char * filename, uint8_t * buf)
{
  app_fd  = open(filename, O_WRONLY | O_CREAT);
  int ret = write(app_fd, buf, 256);
  close(app_fd);
  return ret;
}
