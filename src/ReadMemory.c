#include "WriteAppImage.h"
#include "Commands.h"
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int app_fd = 0;
uint8_t readbuf[256];
uint8_t responsebuf[256];
int numread = 0;
uint32_t addrtrav;

//start address of app must be known here, 0x2000
int WriteAppImage(char * filename)
{
  if( OpenAppImage(filename) == 0 )
    return 2;

  if( GetFileLength() < 0x2000 ) //app too short
    return 3;

  lseek(app_fd,0x2000,SEEK_SET); //seek to start of app

  //read file 256 bytes at a time,
  addrtrav = 0x2000;
  numread = read(app_fd, readbuf, 256);
  while( numread )
  {
    //call Commands_WriteFlash256() on each block
    if( 1 != Commands_SendWriteFlash256(addrtrav,readbuf) )
    {
      return 6;
    }
    if( 2 == Commands_WaitForBytesReceived(responsebuf,2,1000) )
    {
      return 4;
    } 
    if( 1 != responsebuf[1] )
      return 5;

    addrtrav+=256;
    numread = read(app_fd, readbuf, 256);

    usleep(100000);
  }

  return 1;
}


int SeekAppImageToAddress(uint32_t address)
{
  int ret = lseek(app_fd,address,SEEK_SET);
  if( (unsigned int)ret == address )
    return ret;

  return 0;
}

int OpenAppImage(char * filename)
{
  app_fd  = open(filename, O_RDONLY);
  if( app_fd > 0 )
    return app_fd;
  return 0;
}

int CloseAppImage(void)
{
  close(app_fd);
  return 1;
}
int GetFileLength(void)
{
  int size = lseek(app_fd,0L,SEEK_END);
  lseek(app_fd,0,SEEK_SET);
  return size;
}
