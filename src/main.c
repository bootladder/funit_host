#include "Parse_Bootloader_Commandline.h"
#include "LoadFunit.h"
#include "Commands.h"
#include "ttySerial.h"
#include <stdlib.h>
#include <stdio.h>

static int _check_targetaddress(void)
{
    if( Bootloader_TargetAddress == 0 ){
      printf("must supply target address");
      return 0;
    }
    return 1;
}
      

int main(int argc, char * argv[])
{
  if( 1 != Parse_Bootloader_Commandline(argc,argv) )
  {
    printf("invalid commandline\n");
    return 0;
  }
  if( 0 == ttySerialOpen(Bootloader_SerialDevicePath) ){
    printf("Could not open serial device \n");return 0;
  } 

  switch(Bootloader_Command)
  {
    /*  LOAD FUNIT */
    case 'W':
    {
      fprintf(stderr,"Writing Funit to Target\n");
      if( _check_targetaddress() == 0 )
        break;
      int ret = LoadFunit((char*)Bootloader_FunitPath, Bootloader_TargetAddress);
      if( ret != 1 ){
        printf("failed to load funit");
      }

      break;
    }
    /*  EXECUTE FUNIT */
    case 'X':
    {
      fprintf(stderr,"Executing Funit on Target\n");
      if( _check_targetaddress() == 0 )
        break;

      int ret = ExecuteFunit(Bootloader_TargetAddress,Bootloader_ExecuteParam);
      fprintf(stderr,"returned  value from target: %d", ret);
      

      break;
    }
    /*  READ MEMORY */
    case 'R':
    {
      fprintf(stderr,"Reading Funit from Target\n");
      int ret = ReadMemory(Bootloader_TargetAddress,Bootloader_AppImage);
      if( ret == 0 ){
      }

      break;
    }
    case 'q':
    {
      Commands_SendHalt();
      break;
    }
    case '0':
    {
      Commands_SendReset();
      break;
    }
    case 'T':
    {
      Commands_SerialTest();
      break;
    }
    default:
      return 0;
  }

  ttySerialClose() ;

}
/*
    case 'w':
    {

        int ret = WriteAppImage((char*)Bootloader_AppImage) ;
        if( ret == 0 ){
          printf("fail\n");return 0;
        }
        if( ret == 2 ){
          printf("Bad Filename\n");return 0;
        }
        else if( ret == 3){
          printf("Image too short\n");return 0;
        }
        else if( ret == 4){
          printf("Timeout waiting for response\n");return 0;
        }
        else if( ret == 5){
          printf("write page failed\n");return 0;
        }
      
        printf("successfully wrote App Image to Target!\n");
        break;
    }
*/
