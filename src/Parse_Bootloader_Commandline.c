#include "Parse_Bootloader_Commandline.h"
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <getopt.h>
#include <stdio.h>

uint8_t Bootloader_Command;
uint32_t Bootloader_TargetAddress;
uint32_t Bootloader_ExecuteParam;
char Bootloader_AppImage[100];
char Bootloader_FunitPath[100];
char Bootloader_SerialDevicePath[100];

static const char *short_options = "hR:W:d:A:XP:T";

static void print_help(void){

  printf(" (R)ead   (W)rite   (d)evice   (A)ddress    (P)aram    e(X)ecute (T)est serial\n");
  return;
}

int Parse_Bootloader_Commandline(int argc, char * argv[])
{
  int c;
  if( argc == 1)  return 0;

  while ((c = getopt(argc, argv, short_options )) != -1)
    {
    switch (c)
    {
      case 'h': print_help(); break;
      case 'R': 
        memcpy(Bootloader_AppImage,optarg,strlen(optarg)); 
        Bootloader_Command = 'R';
        break;

      case 'W': 
        memcpy(Bootloader_FunitPath,optarg,strlen(optarg));
        Bootloader_Command = 'W';
        break;

      case 'X': 
        Bootloader_Command = 'X';
        break;

      case 'd': 
        memcpy(Bootloader_SerialDevicePath,optarg,strlen(optarg));
        break;

      case 'A': 
        Bootloader_TargetAddress = strtoul(optarg, NULL, 0);
        break;

      case 'P': 
        Bootloader_ExecuteParam = strtoul(optarg, NULL, 0);
        break;

      case 'T': 
        Bootloader_Command = 'T';
        break;

      default:
        break;
    }
    }
  return 1;

      //case 'X': print_help(argv[0]); break;

//  else if( strcmp(argv[1],"-L") == 0){
//    
//  } 
//
//  else if( strcmp(argv[1],"-e") == 0){
//    
//      //valid erasecommand.
//      Bootloader_Command = 'e';
//      return 1;
//  } 
//  else if( strcmp(argv[1],"-R") == 0){
//    
//      Bootloader_Command = 'R';
//      return 1;
//  } 
//  else if( strcmp(argv[1],"-H") == 0){
//    
//      Bootloader_Command = 'H';
//      return 1;
//  } 
//  else
//  {
//    return 2;
//  }
  //Bootloader_Command = 0;
  //Bootloader_AppImage[0] = 0; 
  (void)argc;(void)argv;
  return 0;
}
