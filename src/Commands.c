#include "Commands.h"
#include "ttySerial.h"
#include "Protocol.h"
#include <unistd.h>
#include <arpa/inet.h>
#include <string.h>

int Commands_SendHalt(void)
{
  //type is 1
  uint8_t message[1];
  message[0] = 1;
   
  if( 1 == ttySerialWrite(message,1) )
    return 1;

  return 0;
}

int Commands_SendReset(void)
{
  //type is 2
  uint8_t message[1];
  message[0] = 2;
   
  if( 1 == ttySerialWrite(message,1) )
    return 1;

  return 0;
}
int Commands_SendExecuteFunit(uint32_t addr, uint32_t param)
{
  //type is 5
  uint8_t message[261];
  message[0] = 0x88;
  message[1] = 0x82;

  message[2] = 5;
  //address in network order
  uint32_t networkaddress = htonl(addr);
  for(int i=0;i<4;i++){
    message[3+i] = networkaddress&0xFF;
    networkaddress>>=8; 
  }
  
  //param in network order
  uint32_t networkparam = htonl(param);
  for(int i=0;i<4;i++){
    message[7+i] = networkparam&0xFF;
    networkparam>>=8; 
  }
   
  message[11] = 0x82;
  message[12] = 0x88;

  if( 1 == ttySerialWrite(message,13) )
    return 1;


  (void)addr;
  (void)lengths_of_types;
  return 0;
}
int Commands_SendWriteFlash256(uint32_t addr, uint8_t * buf)
{
  //type is 3
  uint8_t message[261];
  message[0] = 3;
  //address in network order
  uint32_t networkaddress = htonl(addr);
  for(int i=0;i<4;i++){
    message[1+i] = networkaddress&0xFF;
    networkaddress>>=8; 
  }
  memcpy(&(message[5]),buf,256);
   
  if( 1 == ttySerialWrite(message,261) )
    return 1;


  (void)addr;(void)buf;
  (void)lengths_of_types;
  return 0;
}

int Commands_SendReadFlash256(uint32_t addr, uint8_t * buf)
{
  //type is 4
  uint8_t message[5];
  message[0] = 4;
  //address in network order
  uint32_t networkaddress = htonl(addr);
  for(int i=0;i<4;i++){
    message[1+i] = networkaddress&0xFF;
    networkaddress>>=8; 
  }
  if( 1 == ttySerialWrite(message,5) )
    return 1;

  (void)addr;(void)buf;
  return 0;
}
//////////////////////////////////////////////////////

int Commands_WaitForBytesReceived(uint8_t * buf, uint16_t num, uint32_t timeout)
{
  //start timer
  uint16_t numreceived = 0;
  uint16_t sleepcount = 0;
  while(numreceived < num){

    if( 1 == ttySerialRead(buf,1) ){
      numreceived++;
      buf++;
    }
    else{
      usleep(1000);
      sleepcount++;
      if( sleepcount == timeout)
        return 2;
    }

  }
  (void)buf;(void)num;(void)timeout;
  return 1;
}


void Commands_SerialTest(void)
{
  uint8_t message[261];
  for(int i=0;i<11;i++)
  {
    message[i] = 0x0 + i;
  }
  
  while(1) {

  if( 1 != ttySerialWrite(message,11) )
    return;

  }
}
