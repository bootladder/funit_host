#include <stdint.h>


int Parse_Bootloader_Commandline(int argc, char * argv[]);

extern uint8_t Bootloader_Command;
extern uint32_t Bootloader_TargetAddress;
extern uint32_t Bootloader_ExecuteParam;

extern char Bootloader_AppImage[100];
extern char Bootloader_FunitPath[100];
extern char Bootloader_SerialDevicePath[100];
