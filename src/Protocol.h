
#ifndef __PROTOCOL_H_
#define __PROTOCOL_H_

static unsigned int lengths_of_types[16] = {

  [1] = 1,
  [2] = 1,
  [3] = 260,
  [4] = 1,

};

extern char Protocol_ReceivedCommandBuffer[512];
extern unsigned int Protocol_ReceivedCommandType;
extern unsigned int Protocol_ReceivedNewCommand;

#endif
