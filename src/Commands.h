#include <stdint.h>

int Commands_SendHalt(void);
int Commands_SendReset(void);
int Commands_SendExecuteFunit(uint32_t addr, uint32_t param);
int Commands_SendWriteFlash256(uint32_t addr, uint8_t * buf);
int Commands_SendReadFlash256(uint32_t addr, uint8_t * buf);

int Commands_WaitForBytesReceived(uint8_t * buf, uint16_t num, uint32_t timeout);

void Commands_SerialTest(void);
      
